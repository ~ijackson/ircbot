# Actual IRC bot code

set helpfile helpinfos

source irccore.tcl
source parsecmd.tcl
source stdhelp.tcl
source userv.tcl

defset marktime_min 300
defset marktime_join_startdelay 5000

proc privmsg_unlogged {prefix ischan params} {
    if {!$ischan ||
        [regexp {^![a-z][-a-z]*[a-z]( .*)?$} [lindex $params 1]]} {
	return 0
    }
    # on-channel message, ignore
    set chan [lindex $params 0]
    upvar #0 chan_lastactivity([irctolower $chan]) la
    set la [clock seconds]
    catch_logged { recordlastseen_p $prefix "talking on $chan" 2 }
    return 1
}

proc showintervalsecs {howlong abbrev} {
    return [showintervalsecs/[opt timeformat] $howlong $abbrev]
}

proc formatsf {pfx value} {
   foreach {min format} { 100 %.0f 10 %.1f 0 %.2f} {
	set fval [format $format $value]
	if {$fval < $min} continue
	return [format "$fval${pfx}" $value]
   }
}

proc showintervalsecs/beat {howlong abbrev} {
    # We split in two to avoid overflow problems.
    if {$howlong < 86 } {
	# mB's
	set pfx mB
	return [format "%.0fmB" [expr {round($howlong * 1.157)*10} ]]
    } else {
	if {$howlong < 86400 } {
	    # B's
	    set pfx B
	    set value [expr {$howlong / 86.4}]
	} else {
	    # kB's
	    set pfx kB
	    set value [expr {$howlong / 86400.0}]
	}
    }
    return [formatsf $pfx $value]
}

proc showintervalsecs/ks {howlong abbrev} {
    if {$howlong < 1000} {
	return "${howlong}s"
    } else {
	if {$howlong < 1000000} {
	    set pfx ks
	    set scale 1000
	} else {
	    set pfx Ms
	    set scale 1000000
	}
	set value [expr "$howlong.0 / $scale"]
	return [formatsf $pfx $value]
    }
}

proc format_qty {qty unit abbrev} {
    set o $qty
    if {$abbrev} {
	append o [string range $unit 0 0]
    } else {
	append o " "
	append o $unit
	if {$qty != 1} { append o s }
    }
    return $o
}

proc showintervalsecs/hms {qty abbrev} {
    set ul {second 60 minute 60 hour 24 day 7 week}
    set remainv 0
    while {[llength $ul] > 1 && $qty >= [set uv [lindex $ul 1]]} {
	set remainu [lindex $ul 0]
	set remainv [expr {$qty % $uv}]
	set qty [expr {($qty-$remainv)/$uv}]
	set ul [lreplace $ul 0 1]
    }
    set o [format_qty $qty [lindex $ul 0] $abbrev]
    if {$remainv} {
	if {!$abbrev} { append o " " }
	append o [format_qty $remainv $remainu $abbrev]
    }
    return $o
}

proc showinterval {howlong} {
    if {$howlong <= 0} {
	return {just now}
    } else {
	return "[showintervalsecs $howlong 0] ago"
    }
}

proc showtime {when} {
    return [showinterval [expr {[clock seconds] - $when}]]
}

proc parse_interval {specified min} {
    if {![regexp {^([0-9]+)([a-z]+)$} $specified dummy value unit]} {
	error "invalid syntax for interval"
    }
    switch -exact $unit {
	s { set u 1 }
	ks { set u 1000 }
	m { set u 60 }
	h { set u 3600 }
	mb { set u 0.0864 }
	b { set u 86.4 }
	kb { set u 86400 }
	default { error "unknown unit of time $unit" }
    }
    if {$value > 86400*21/$u} { error "interval too large" }
    set result [expr {round($value*$u)}]
    if {$result < $min} { error "interval too small (<${min}s)" }
    return $result
}

proc def_msgproc {name argl body} {
    proc msg_$name "varbase $argl" "\
    upvar #0 msg/\$varbase/dest d\n\
    upvar #0 msg/\$varbase/str s\n\
    upvar #0 msg/\$varbase/accum a\n\
$body"
}

def_msgproc begin {dest str} {
    set d $dest
    set s $str
    set a {}
}

def_msgproc append {str} {
    set ns "$s$str"
    if {[string length $s] && [string length $ns] > 65} {
	msg__sendout $varbase
	set s " [string trimleft $str]"
    } else {
	set s $ns
    }
}

def_msgproc finish {} {
    msg__sendout $varbase
    unset s
    unset d
    return $a
}

def_msgproc _sendout {} {
    lappend a [string trimright $s]
    set s {}
}

proc looking_whenwhere {when where} {
    set str [showtime [expr {$when-1}]]
    if {[string length $where]} { append str " on $where" }
    return $str
}

proc tell_getcstate {} {
    # uses nl from caller's context
    # imports telling (as the nick_telling) and u
    # sets stt, telling_when
    uplevel 1 {
	upvar #0 nick_telling($nl) telling
	upvar #0 nick_unique($nl) u

	if {[info exists telling]} {
	    manyset $telling u_last stt telling_when
	    if {![info exists u] || "$u_last" != "$u"} {
		set stt undelivered
	    }
	} else {
	    set stt undelivered
	    set telling_when 0
	}
    }
}

proc tell_event {nl event} {
    # For `act' we *haven't* yet done the 750ms delay; we implement
    # that here.  Also, here we turn `talk' into `talk' now and `act'
    # later.  We also support the psuedo-event `none'.  The del msg
    # and new msg events are handled by the command procedures, not here.
    global calling_nick
    if {[info exists calling_nick]} { set save $calling_nick }
    set r DELAYED
    switch -exact $event {
	none { }
	talk {
	    tell_event_core $nl talk
	    tell_event $nl act
	}
	act {
	    after 750 [list tell_event_core $nl $event]
	}
	ident - msgsarrive {
	    tell_event_core $nl $event
	}
	tellme {
	    set r [tell_event_core $nl TELLME]
	}
	default {
	    error "tell_event $nl $event"
	}
    }
    if {[info exists save]} { set calling_nick $save }
    return $r
}

proc tell_event_core {nl event} {
    global tell_event_teventi errorInfo
    set tell_event_teventi "*$event* $nl"
    if {[catch {
	tell_event_core1 $nl $event
    } emsg]} {
	log_intern "tell event error" "$emsg >$errorInfo<"
	set emsg ERROR
    } else {
	if {"$emsg" != "nomsgs"} {
	    log_intern "tell event" "done $tell_event_teventi $emsg"
	}
    }
    return $emsg
}

proc tell_event_core1 {nl event} {
    # event is `talk', `act', `ident', `msgsarrive' or `TELLME'
    # When user talks we actually get talk now and act later
    global calling_nick
    set calling_nick $nl
    set iml [msgsdb_get $nl inbound]
    if {![llength $iml]} { return nomsgs }

    set now [clock seconds]
    tell_getcstate
    set ago [expr {$now - $telling_when}]

    # Now we have the components of a telling state
    #   u     - nick_unique (unset if not visible)
    #   stt   - state: undelivered, mentioned, passede
    #   ago   - how long ago since we did anything

    # We compute an evstate to dispatch on as follows:

    # evstate is string of letters
    #   current state
    #      u   UNDELIVERED (MESSAGES)
    #      m   MENTIONED
    #      p   PASSED
    #   event
    #      t   talk
    #      a   act
    #      i   ident
    #      m   msgsarrive
    #      T   tellme (user command)
    #   security level and timing
    #      ii  Insecure
    #      ss  Secure and soon (before interval)
    #      sl  Secure and late (after interval)
    #   current identification
    #      i   Identified
    #      u   Unidentified
    #   reliability and timing
    #      uu  Unreliable
    #      rv  Remind, very soon (before within-interval)
    #      rs  Remind, soon (between)
    #      rl  Remind, late (after every-interval)
    #      ps  Pester, soon (before interval)
    #      pl  Pester, late (after interval)

    set evstate {}

    append evstate [string range $stt 0 0]
    append evstate [string range $event 0 0]

    manyset [nickdb_get_sec_effective $nl] sec secwhen
    switch -exact $sec {
	insecure { append evstate ii }
	secure { append evstate [expr {$ago<$secwhen ? "ss" : "sl"}] }
	default { append evstate "#$sec#" }
    }

    upvar #0 nick_username($nl) nu
    if {[info exists nu] && ![string compare $nu [nickdb_get_username $nl]]} {
	append evstate i
    } else {
	append evstate u
    }
    
    manyset [nickdb_get $nl tellrel] rel relint relwithin
    switch -exact $rel {
	unreliable { append evstate uu }
	remind { append evstate [expr {
	    $ago<$relwithin ? "rv" : $ago<$relint ? "rs" : "rl"
	}]}
	pester { append evstate [expr {$ago<$relint ? "ps" : "pl"}] }
	default { append evstate "#$rel#" }
    }

    global tell_event_teventi
    set tell_event_teventi "$evstate $ago $nl"
    switch -glob $evstate {
	pt???rv {
	    # consider delivered:
	    #  (very recently passed, and the user talks)
	    set ndel [tell_delete_msgs {} $nl]
	    sendprivmsg $nl \
 "I'm assuming you got the $ndel message(s) I just passed on."
	    return delivered
	}
	pm????? {
	    # oops, messages passed are now out of date
	    catch_restoreei { unset telling }
	    return reset
	}
	?m????? {
	    # ignore new msgs if we haven't passed yet
	    return ignorenew
	}
	ut????? - mt????? -
	pt???uu - pt???rs - pt???rl - pt???p? {
	    # ignore (any other `talk's) - act handles these
	    return ignoretalk
	}
	ui????? -
	uaii?uu - uaii??l - uas?iuu - uas?i?l -
	mi????? - pa????l -
	?Tii??? - ?Ts?i?? {
	    # pass and then stuff
	    if {[llength $iml] == 3} {
		manyset $iml sender sentwhen msg
		sendprivmsg $nl \
 "$sender asked me, [showinterval [expr {$now-$sentwhen}]],\
 to tell you: $msg"
            } else {
		sendprivmsg $nl \
 "I have [expr {[llength $iml]/3}] messages for you:"
		while {[llength $iml] >= 3} {
		    manyset [lrange $iml 0 2] sender sentwhen msg
		    set iml [lrange $iml 3 end]
		    sendprivmsg $nl \
 " [showintervalsecs [expr {$now-$sentwhen}] 1] <$sender> $msg"
		}
	    }
	    if {![string compare $rel "unreliable"]} {
		tell_delete_msgs {} $nl
		return toldunreliable
	    }
	    set stt passed
	    set re passed
	}
	uaslu?? {
	    sendprivmsg $nl {You have messages (so identify yourself please).}
	    set stt mentioned
	    set re mentioned
	}
	?Ts?u?? {
	    sendprivmsg $nl {You must identify yourself to see your messages.}
	    return ignoreuitm
	}
	masl??? {
	    sendprivmsg $nl {Don't forget about your messages.}
	    set re remind
	}
	pi????? {
	    return ignorepi
	}
	mass??? - pa????v - pa????s -
	uaii??v - uaii??s -
	uas?i?v - uas?i?s -
	uassu?? {
	    # too soon
	    return ignoresoon
	}
	* {
	    error "tell_event_core nl=$nl evstate=$evstate ?"
	}
    }
    if {![info exists u]} {
	set telling [list {} undelivered $now]
    } else {
	set telling [list $u $stt $now]
    }
    return $re
}

proc recordlastseen_n {n how here} {
    # here is:
    #   0 - nick was seen leaving (or changing to another nicks or some such)
    #   1 - nick was seen doing something else
    #   2 - nick was seen talking on channel
    global lastseen lookedfor
    set nl [irctolower $n]
    set now [clock seconds]
    set lastseen($nl) [list $n $now $how]

    if {!$here} return

    tell_event $nl [lindex {none act talk} $here]

    upvar #0 lookedfor($nl) lf
    if {[info exists lf]} {
	switch -exact [llength $lf] {
	    0 {
		set ml {}
	    }
	    1 {
		manyset [lindex $lf 0] when who where
		set ml [list \
 "FYI, $who was looking for you [looking_whenwhere $when $where]."]
	    }
	    default {
		msg_begin tosend $n "FYI, people have been looking for you:"
		set i 0
		set fin ""
		foreach e $lf {
		    incr i
		    if {$i == 1} {
			msg_append tosend " "
		    } elseif {$i == [llength $lf]} {
			msg_append tosend " and "
			set fin .
		    } else {
			msg_append tosend ", "
		    }
		    manyset $e when who where
		    msg_append tosend \
			    "$who ([looking_whenwhere $when $where])$fin"
		}
		set ml [msg_finish tosend]
	    }
	}
	unset lf
	msendprivmsg_delayed 1000 $n $ml
    }
}

proc note_topic {showoff whoby topic} {
    set msg "FYI, $whoby has changed the topic on $showoff"
    if {[string length $topic] < 160} {
	append msg " to $topic"
    } else {
	append msg " but it is too long to reproduce here !"
    }
    set showoff [irctolower $showoff]
    set tell [chandb_get $showoff topictell]
    if {[lsearch -exact $tell *] >= 0} {
	set tryspies [chandb_list]
    } else {
	set tryspies $tell
    }
    foreach spy $tryspies {
	set see [chandb_get $spy topicsee]
	if {[lsearch -exact $see $showoff] >= 0 || \
		([lsearch -exact $see *] >= 0 && \
		[lsearch -exact $tell $spy] >= 0)} {
	    sendprivmsg $spy $msg
	}
    }
}

proc recordlastseen_p {p how here} {
    prefix_nick
    recordlastseen_n $n $how $here
}

proc chanmode_arg {} {
    upvar 2 args cm_args
    set rv [lindex $cm_args 0]
    set cm_args [lreplace cm_args 0 0]
    return $rv
}

proc chanmode_o1 {m g p chan} {
    global nick chan_initialop
    prefix_nick
    set who [chanmode_arg]
    recordlastseen_n $n "being nice to $who" 1
    if {![ircnick_compare $who $nick]} {
	set nlower [irctolower $n]
	upvar #0 nick_unique($nlower) u
	if {[chandb_exists $chan]} {
	    sendprivmsg $n Thanks.
	} elseif {![info exists u]} {
	    sendprivmsg $n {Op me while not on the channel, why don't you ?}
	} else {
	    set chan_initialop([irctolower $chan]) $u
	    sendprivmsg $n \
 "Thanks.  You can use `channel manager ...' to register this channel."
	    if {![string length [nickdb_get_username $n username]]} {
		sendprivmsg $n \
 "(But to do that you must register your nick securely first.)"
	    }
	}
    }
}

proc chanmode_o0 {m g p chan} {
    global nick chandeop
    prefix_nick
    set who [chanmode_arg]
    recordlastseen_p $p "being mean to $who" 1
    if {![ircnick_compare $who $nick]} {
	set chandeop($chan) [list [clock seconds] $p]
    }
}

proc msg_MODE {p c dest modelist args} {
    if {![ischan $dest]} return
    if {[regexp {^\-(.+)$} $modelist dummy modelist]} {
	set give 0
    } elseif {[regexp {^\+(.+)$} $modelist dummy modelist]} {
	set give 1
    } else {
	error "invalid modelist"
    }
    foreach m [split $modelist] {
	set procname chanmode_$m$give
	if {[catch { info body $procname }]} {
	    recordlastseen_p $p "fiddling with $dest" 1
	} else {
	    $procname $m $give  $p $dest
	}
    }
}

proc leaving {lchan} {
    global nick_onchans
    foreach luser [array names nick_onchans] {
	upvar #0 nick_onchans($luser) oc
	set oc [grep tc {"$tc" != "$lchan"} $oc]
    }
    upvar #0 chan_nicks($lchan) nlist
    unset nlist
    upvar #0 chan_lastactivity($lchan) la
    catch { unset la }
}

proc doleave {lchan} {
    sendout PART $lchan
    leaving $lchan
}

proc dojoin {lchan} {
    global chan_nicks
    sendout JOIN $lchan
    set chan_nicks($lchan) {}
}

proc check_justme {lchan} {
    global nick
    upvar #0 chan_nicks($lchan) nlist
    if {[llength $nlist] != 1} return
    if {"[lindex $nlist 0]" != "[irctolower $nick]"} return
    if {[chandb_exists $lchan]} {
	set mode [chandb_get $lchan mode]
	if {"$mode" != "*"} {
	    sendout MODE $lchan $mode
	}
	set topic [chandb_get $lchan topicset]
	if {[string length $topic]} {
	    sendout TOPIC $lchan $topic
	}
    } else {
	doleave $lchan
    }
}

proc process_kickpart {chan user} {
    global nick
    check_nick $user
    set luser [irctolower $user]
    set lchan [irctolower $chan]
    if {![ischan $chan]} { error "not a channel" }
    if {![ircnick_compare $luser $nick]} {
	leaving $lchan
    } else {
	upvar #0 nick_onchans($luser) oc
	upvar #0 chan_nicks($lchan) nlist
	set oc [grep tc {"$tc" != "$lchan"} $oc]
	set nlist [grep tn {"$tn" != "$luser"} $nlist]
	nick_case $user
	if {![llength $oc]} {
	    nick_forget $luser
	} else {
	    check_justme $lchan
	}
    }
}

proc msg_TOPIC {p c dest topic} {
    prefix_nick
    if {![ischan $dest]} return
    recordlastseen_n $n "changing the topic on $dest" 1
    note_topic [irctolower $dest] $n $topic
}

proc msg_KICK {p c chans users comment} {
    set chans [split $chans ,]
    set users [split $users ,]
    if {[llength $chans] > 1} {
	foreach chan $chans user $users { process_kickpart $chan $user }
    } else {
	foreach user $users { process_kickpart [lindex $chans 0] $user }
    }
}

proc msg_KILL {p c user why} {
    nick_forget $user
}

set nick_counter 0
set nick_arys {onchans username unique}
# nick_onchans($luser) -> [list ... $lchan ...]
# nick_username($luser) -> <securely known local username>
# nick_unique($luser) -> <includes-counter>
# nick_case($luser) -> $user  (valid even if no longer visible)
# nick_markid($luser) -> <after id for marktime>
# nick_telling($luser) -> <unique> mentioned|passed <when>

# chan_nicks($lchan) -> [list ... $luser ...]
# chan_lastactivity($lchan) -> [clock seconds]

proc lnick_forget {luser} {
    global nick_arys chan_nicks
    lnick_marktime_cancel $luser
    foreach ary $nick_arys {
	upvar #0 nick_${ary}($luser) av
	catch { unset av }
    }
    foreach lch [array names chan_nicks] {
	upvar #0 chan_nicks($lch) nlist
	set nlist [grep tn {"$tn" != "$luser"} $nlist]
	check_justme $lch
    }
}

proc nick_forget {user} {
    global nick_arys chan_nicks
    lnick_forget [irctolower $user]
    nick_case $user
}

proc nick_case {user} {
    global nick_case
    set nick_case([irctolower $user]) $user
}

proc msg_NICK {p c newnick} {
    global nick_arys nick_case calling_nick
    prefix_nick
    recordlastseen_n $n "changing nicks to $newnick" 0
    set calling_nick $newnick
    recordlastseen_n $newnick "changing nicks from $n" 1
    set luser [irctolower $n]
    lnick_marktime_cancel $luser
    set lusernew [irctolower $newnick]
    foreach ary $nick_arys {
	upvar #0 nick_${ary}($luser) old
	upvar #0 nick_${ary}($lusernew) new
	if {[info exists new]} { error "nick collision ?! $ary $n $newnick" }
	if {[info exists old]} { set new $old; unset old }
    }
    upvar #0 nick_onchans($lusernew) oc
    foreach ch $oc {
	upvar #0 chan_nicks($ch) nlist
	set nlist [grep tn {"$tn" != "$luser"} $nlist]
	lappend nlist $lusernew
    }
    lnick_marktime_start $lusernew "Hi." 500 1
    nick_case $newnick
}

proc nick_ishere {n} {
    global nick_counter
    upvar #0 nick_unique([irctolower $n]) u
    if {![info exists u]} { set u [incr nick_counter].$n.[clock seconds] }
    nick_case $n
}

proc msg_JOIN {p c chan} {
    prefix_nick
    nick_ishere $n
    recordlastseen_n $n "joining $chan" 1
    set nl [irctolower $n]
    set lchan [irctolower $chan]
    upvar #0 nick_onchans($nl) oc
    upvar #0 chan_nicks($lchan) nlist
    if {![info exists oc]} {
	global marktime_join_startdelay
	lnick_marktime_start $nl "Welcome." $marktime_join_startdelay 1
    }
    lappend oc $lchan
    lappend nlist $nl
}
proc msg_PART {p c chan args} {
    prefix_nick
    set msg "leaving $chan"
    if {[llength $args]} {
	set why [lindex $args 0]
	if {"[irctolower $why]" != "[irctolower $n]"} {	append msg " ($why)" }
    }
    recordlastseen_n $n $msg 1
    process_kickpart $chan $n
}
proc msg_QUIT {p c why} {
    prefix_nick
    recordlastseen_n $n "leaving ($why)" 0
    nick_forget $n
}

proc msg_PRIVMSG {p c dest text} {
    global errorCode
    
    prefix_nick
    if {[ischan $dest]} {
	recordlastseen_n $n "invoking me in $dest" 1
	set output $dest
    } else {
	recordlastseen_n $n "talking to me" 1
	set output $n
    }
    nick_case $n

    execute_usercommand $p $c $n $output $dest $text
}

proc msg_INVITE {p c n chan} {
    after 1000 [list dojoin [irctolower $chan]]
}

proc grep {var predicate list} {
    set o {}
    upvar 1 $var v
    foreach v $list {
	if {[uplevel 1 [list expr $predicate]]} { lappend o $v }
    }
    return $o
}

proc msg_353 {p c dest type chan nicklist} {
    global names_chans nick_onchans
    set lchan [irctolower $chan]
    upvar #0 chan_nicks($lchan) nlist
    lappend names_chans $lchan
    if {![info exists nlist]} {
	# We don't think we're on this channel, so ignore it !
	# Unfortunately, because we don't get a reply to PART,
	# we have to remember ourselves whether we're on a channel,
	# and ignore stuff if we're not, to avoid races.  Feh.
	return
    }
    set nlist_new {}
    foreach user [split $nicklist { }] {
	regsub {^[@+]} $user {} user
	if {![string length $user]} continue
	check_nick $user
	set luser [irctolower $user]
	upvar #0 nick_onchans($luser) oc
	lappend oc $lchan
	lappend nlist_new $luser
	nick_ishere $user
    }
    set nlist $nlist_new
}

proc msg_366 {p c args} {
    global names_chans nick_onchans
    set lchan [irctolower $c]
    foreach luser [array names nick_onchans] {
	upvar #0 nick_onchans($luser) oc
	if {[llength names_chans] > 1} {
	    set oc [grep tc {[lsearch -exact $tc $names_chans] >= 0} $oc]
	}
	if {![llength $oc]} { lnick_forget $luser }
    }
    unset names_chans
}

proc somedb__head {} {
    uplevel 1 {
	set idl [irctolower $id]
	upvar #0 ${nickchan}db($idl) ndbe
	binary scan $idl H* idh
	set idfn $fprefix$idh
	if {![info exists iddbe] && [file exists $idfn]} {
	    set f [open $idfn r]
	    try_except_finally { set newval [read $f] } {} { close $f }
	    if {[llength $newval] % 2} { error "invalid length" }
	    set iddbe $newval
	}
    }
}

proc def_somedb {name arglist body} {
    foreach {nickchan fprefix} {
	nick users/n
	chan chans/c
	msgs users/m
    } {
	proc ${nickchan}db_$name $arglist \
            "set nickchan $nickchan; set fprefix $fprefix; $body"
    }
}

def_somedb list {} {
    set list {}
    foreach path [glob -nocomplain -path $fprefix *] {
	binary scan $path "A[string length $fprefix]A*" afprefix thinghex
	if {"$afprefix" != "$fprefix"} { error "wrong prefix $path $afprefix" }
	lappend list [binary format H* $thinghex]
    }
    return $list
}

proc def_somedb_id {name arglist body} {
    def_somedb $name [concat id $arglist] "somedb__head; $body"
}

def_somedb_id exists {} {
    return [info exists iddbe]
}

def_somedb_id delete {} {
    catch { unset iddbe }
    file delete $idfn
}

set default_settings_nick {
    timeformat ks
    marktime off
    tellsec {secure 600}
    tellrel {remind 3600 30}
}

set default_settings_chan {
    autojoin 1
    mode *
    userinvite pub
    topicset {}
    topicsee {}
    topictell {}
}

set default_settings_msgs {
    inbound {}
    outbound {}
}
# inbound -> [<nick> <time_t> <message>] ...
# outbound -> [<nick> <time_t(earliest)> <count>] ...
#   neither are sorted particularly; only one entry per recipient in
#   output; both sender and recipient are cased

def_somedb_id set {args} {
    upvar #0 default_settings_$nickchan def
    if {![info exists iddbe]} { set iddbe $def }
    foreach {key value} [concat $iddbe $args] { set a($key) $value }
    set newval {}
    foreach {key value} [array get a] { lappend newval $key $value }
    set f [open $idfn.new w]
    try_except_finally {
	puts $f $newval
	close $f
	file rename -force $idfn.new $idfn
    } {
    } {
	catch { close $f }
    }
    set iddbe $newval
}

def_somedb_id get {key} {
    upvar #0 default_settings_$nickchan def
    if {[info exists iddbe]} {
	set l [concat $iddbe $def]
    } else {
	set l $def
    }
    foreach {tkey value} $l {
	if {![string compare $tkey $key]} { return $value }
    }
    error "unset setting $key"
}

proc opt {key} {
    global calling_nick
    if {[info exists calling_nick]} { set n $calling_nick } { set n {} }
    return [nickdb_get $n $key]
}

proc check_notonchan {} {
    upvar 1 dest dest
    if {[ischan $dest]} { usererror "That command must be sent privately." }
}

proc nick_securitycheck {strict} {
    upvar 1 n n
    if {![nickdb_exists $n]} {
	usererror "You are unknown to me, use `register'."
    }
    set wantu [nickdb_get $n username]
    if {![string length $wantu]} {
	if {$strict} {
	    usererror "That feature is only available to secure users, sorry."
	} else {
	    return
	}
    }
    set luser [irctolower $n]
    upvar #0 nick_username($luser) nu
    if {![info exists nu]} {
	usererror "Nick $n is secure, you must identify yourself first."
    }
    if {"$wantu" != "$nu"} {
	usererror "You are the wrong user -\
		the nick $n belongs to $wantu, not $nu."
    }
}

proc channel_ismanager {channel n} {
    set mgrs [chandb_get $channel managers]
    return [expr {[lsearch -exact [irctolower $mgrs] [irctolower $n]] >= 0}]
}

proc channel_securitycheck {channel} {
    upvar n n
    if {![channel_ismanager $channel $n]} {
	usererror "You are not a manager of $channel."
    }
    nick_securitycheck 1
}

proc def_chancmd {name body} {
    proc channel/$name {} \
	    "    upvar 1 target chan; upvar 1 n n; upvar 1 text text; $body"
}

proc ta_listop {findnow procvalue} {
    # findnow and procvalue are code fragments which will be executed
    # in the caller's level.  findnow should set ta_listop_ev to
    # the current list, and procvalue should treat ta_listop_ev as
    # a proposed value in the list and check and possibly modify
    # (canonicalise?) it.  After ta_listop, ta_listop_ev will
    # be the new value of the list.
    upvar 1 ta_listop_ev exchg
    upvar 1 text text
    set opcode [ta_word]
    switch -exact _$opcode {
	_= { }
	_+ - _- {
	    uplevel 1 $findnow
	    foreach item $exchg { set array($item) 1 }
	}
	default {
	    error "list change opcode must be one of + - ="
	}
    }
    foreach exchg [split $text " "] {
	if {![string length $exchg]} continue
	uplevel 1 $procvalue
	if {"$opcode" != "-"} {
	    set array($exchg) 1
	} else {
	    catch { unset array($exchg) }
	}
    }
    set exchg [lsort [array names array]]
}

def_chancmd manager {
    ta_listop {
	if {[chandb_exists $chan]} {
	    set ta_listop_ev [chandb_get $chan managers]
	} else {
	    set ta_listop_ev [list [irctolower $n]]
	}
    } {
	check_nick $ta_listop_ev
	set ta_listop_ev [irctolower $ta_listop_ev]
    }
    if {[llength $ta_listop_ev]} {
	chandb_set $chan managers $ta_listop_ev
	ucmdr "Managers of $chan: $ta_listop_ev" {}
    } else {
	chandb_delete $chan
	ucmdr {} {} "forgets about managing $chan." {}
    }
}

def_chancmd autojoin {
    set yesno [ta_word]
    switch -exact [string tolower $yesno] {
	no { set nv 0 }
	yes { set nv 1 }
	default { error "channel autojoin must be `yes' or `no' }
    }
    chandb_set $chan autojoin $nv
    ucmdr [expr {$nv ? "I will join $chan when I'm restarted " : \
	    "I won't join $chan when I'm restarted "}] {}
}

def_chancmd userinvite {
    set nv [string tolower [ta_word]]
    switch -exact $nv {
	pub { set txt "!invite will work for $chan, but it won't work by /msg" }
	here { set txt "!invite and /msg invite will work, but only for users who are already on $chan." }
	all { set txt "Any user will be able to invite themselves or anyone else to $chan." }
	none { set txt "I will not invite anyone to $chan." }
	default {
	    error "channel userinvite must be `pub', `here', `all' or `none'
	}
    }
    chandb_set $chan userinvite $nv
    ucmdr $txt {}
}

def_chancmd topic {
    set what [ta_word]
    switch -exact $what {
	leave {
	    ta_nomore
	    chandb_set $chan topicset {}
	    ucmdr "I won't ever change the topic of $chan." {}
	}
	set {
	    set t [string trim $text]
	    if {![string length $t]} {
		error "you must specific the topic to set"
	    }
	    chandb_set $chan topicset $t
	    ucmdr "Whenever I'm alone on $chan, I'll set the topic to $t." {}
	}
	see - tell {
	    ta_listop {
		set ta_listop_ev [chandb_get $chan topic$what]
	    } {
		if {"$ta_listop_ev" != "*"} {
		    if {![ischan $ta_listop_ev]} {
			error "bad channel \`$ta_listop_ev' in topic $what"
		    }
		    set ta_listop_ev [irctolower $ta_listop_ev]
		}
	    }
	    chandb_set $chan topic$what $ta_listop_ev
	    ucmdr "Topic $what list for $chan: $ta_listop_ev" {}
	}
	default {
	    usererror "Unknown channel topic subcommand - see help channel."
	}
    }
}

def_chancmd mode {
    set mode [ta_word]
    if {"$mode" != "*" && ![regexp {^(([-+][imnpst]+)+)$} $mode mode]} {
	error {channel mode must be * or match ([-+][imnpst]+)+}
    }
    chandb_set $chan mode $mode
    if {![string compare $mode "*"]} {
	ucmdr "I won't ever change the mode of $chan." {}
    } else {
	ucmdr "Whenever I'm alone on $chan, I'll set the mode to $mode." {}
    }
}

def_chancmd show {
    if {[chandb_exists $chan]} {
	set l "Settings for $chan: autojoin "
	append l [lindex {no yes} [chandb_get $chan autojoin]]
	append l ", mode " [chandb_get $chan mode]
	append l ", userinvite " [chandb_get $chan userinvite] "."
	append l "\nManagers: "
	append l [join [chandb_get $chan managers] " "]
	foreach {ts sep} {see "\n" tell "  "} {
	    set t [chandb_get $chan topic$ts]
	    append l $sep
	    if {[llength $t]} {
		append l "Topic $ts list: $t."
	    } else {
		append l "Topic $ts list is empty."
	    }
	}
	append l "\n"
	set t [chandb_get $chan topicset]
	if {[string length $t]} {
	    append l "Topic to set: $t"
	} else {
	    append l "I will not change the topic."
	}
	ucmdr {} $l
    } else {
	ucmdr {} "The channel $chan is not managed."
    }
}

proc channelmgr_monoop {} {
    upvar 1 dest dest
    upvar 1 text text
    upvar 1 n n
    upvar 1 p p
    upvar 1 target target
    global chan_nicks

    prefix_nick

    if {[ischan $dest]} { set target $dest }
    if {[ta_anymore]} { set target [ta_word] }
    ta_nomore
    if {![info exists target]} {
	usererror "You must specify, or invoke me on, the relevant channel."
    }
    if {![info exists chan_nicks([irctolower $target])]} {
	usererror "I am not on $target."
    }
    if {![ischan $target]} { error "not a valid channel" }

    if {![chandb_exists $target]} {
	usererror "$target is not a managed channel."
    }
    channel_securitycheck $target
}

def_ucmd op {
    channelmgr_monoop
    sendout MODE $target +o $n
    ucmdr {} {}
}

def_ucmd leave {
    channelmgr_monoop
    doleave $target
    ucmdr {} {}
}

def_ucmd invite {
    global chan_nicks errorCode errorInfo
    prefix_nick
    
    if {[ischan $dest]} {
	set target $dest
	set onchan 1
    } else {
	set target [ta_word]
	set onchan 0
    }
    set ltarget [irctolower $target]
    if {![ischan $target]} { error "$target is not a channel" }
    if {![info exists chan_nicks($ltarget)]} {
	usererror "I am not on $target."
    }
    set ui [chandb_get $ltarget userinvite]
    if {[catch {
	if {![string compare $ui "pub"] && !$onchan} {
	    usererror "Invitations to $target must be made there with !invite."
	}
	if {"$ui" != "all"} {
	    if {[lsearch -exact $chan_nicks($ltarget) [irctolower $n]] < 0} {
		usererror "Invitations to $target may only be made\
			by a user on the channel."
	    }
	}
	if {[!string compare $ui "none"]} {
	    usererror "Sorry, I've not been authorised\
		    to invite people to $target."
	}
    } emsg]} {
	if {![string compare $errorCode "BLIGHT USER"] &&
	     [channel_ismanager $target $n]} {
	    if {[catch {
		nick_securitycheck 1
	    } emsg2]} {
		if {![string compare $errorCode "BLIGHT USER"]} {
		    usererror "$emsg2  Therefore you can't use your\
			    channel manager privilege.  $emsg"
		} else {
		    error $error $errorInfo $errorCode
		}
	    }
	} else {
	    error $emsg $errorInfo $errorCode
	}
    }
    if {![ta_anymore]} {
	usererror "You have to say who to invite."
    }
    set invitees {}
    while {[ta_anymore]} {
	set invitee [ta_nick]
	lappend invitees $invitee
    }
    foreach invitee $invitees {
	sendout INVITE $invitee $ltarget
    }
    set who [lindex $invitees 0]
    switch -exact llength $invitees {
	0 { error "zero invitees" }
	1 { }
	2 { append who " and [lindex $invitees 1]" }
	* {
	    set who [join [lreplace $invitees end end] ", "]
	    append who " and [lindex $invitees [llength $invitees]]"
	}
    }
    ucmdr {} {} {} "invites $who to $target."
}

def_ucmd channel {
    if {[ischan $dest]} { set target $dest }
    if {![ta_anymore]} {
	set subcmd show
    } else {
	set subcmd [ta_word]
    }
    if {[ischan $subcmd]} {
	set target $subcmd
	if {![ta_anymore]} {
	    set subcmd show
	} else {
	    set subcmd [ta_word]
	}
    }
    if {![info exists target]} { error "privately, you must specify a channel" }
    set procname channel/$subcmd
    if {"$subcmd" != "show"} {
	if {[catch { info body $procname }]} {
	    usererror "unknown channel setting $subcmd."
	}
	prefix_nick
	if {[chandb_exists $target]} {
	    channel_securitycheck $target
	} else {
	    nick_securitycheck 1
	    upvar #0 chan_initialop([irctolower $target]) io
	    upvar #0 nick_unique([irctolower $n]) u
	    if {![info exists io]} {
		usererror "$target is not a managed channel."
	    }
	    if {"$io" != "$u"} {
		usererror "You are not the interim manager of $target."
	    }
	    if {"$subcmd" != "manager"} {
		usererror "Please use `channel manager' first."
	    }
	}
    }
    channel/$subcmd
}

proc nickdb_get_username {n} {
    if {![nickdb_exists $n]} { return "" }
    return [nickdb_get $n username]
}

proc nickdb_get_sec_effective {n} {
    set l [nickdb_get $n tellsec]
    set u [nickdb_get_username $n]
    if {![string compare [lindex $l 0] "secure"] &&
        ![string length $u]} { set l insecure }
    return $l
}

proc tell_peernicks {text} {
    global errorInfo errorCode
    set text [irctolower [string trim $text]]
    set senders [split $text " "]
    foreach sender $senders {
	if {[catch { check_nick $sender } emsg]} {
	    error "invalid nick `$sender': $emsg" $errorInfo $errorCode
	}
    }
    return $senders
}

proc msgsdb_set_maydelete {n key l otherkey} {
    msgsdb_set $n $key $l
    if {[llength $l]} return
    if {[llength [msgsdb_get $n $otherkey]]} return
    msgsdb_delete $n
}

proc tell_delete_msgs {lsenders lrecip} {
    set new_inbound {}
    set ndel 0
    foreach {s t m} [msgsdb_get $lrecip inbound] {
	if {[llength $lsenders]} {
	    if {[lsearch -exact $lsenders [irctolower $s]] == -1} {
		lappend new_inbound $s $t $m
		continue
	    }
	}
	set rsenders($s) 1
	incr ndel
    }
    msgsdb_set_maydelete $lrecip inbound $new_inbound outbound
    if {![llength $new_inbound]} {
	upvar #0 nick_telling($lrecip) telling
	catch { unset telling }
    }
    foreach s [array names rsenders] {
	set new_outbound {}
	foreach {r t c} [msgsdb_get $s outbound] {
	    if {![ircnick_compare $r $lrecip]} continue
	    lappend new_outbound $r $t $c
	}
	msgsdb_set_maydelete $s outbound $new_outbound inbound
    }
    return $ndel
}

def_ucmd untell {
    prefix_nick
    check_notonchan
    if {[nickdb_exists $n]} { nick_securitycheck 0 }
    set recipients [tell_peernicks $text]
    if {![llength $recipients]} {
	usererror "You must say which recipients' messages from you to forget."
    }
    set ndel 0
    foreach recip $recipients {
	incr ndel [tell_delete_msgs [irctolower $n] $recip]
    }
    ucmdr "Removed $ndel as yet undelivered message(s)." {}
}

def_ucmd_alias delmsgs delmsg
def_ucmd delmsg {
    global errorInfo errorCode
    prefix_nick
    set nl [irctolower $n]
    check_notonchan
    manyset [nickdb_get_sec_effective $n] sec secwhen
    switch -exact $sec {
	insecure { }
	reject - mailto {
	    usererror \
 "There are no messages to delete\
 because your message disposition prevents them from being left."
	}
	secure {
	    nick_securitycheck 1
	}
	default {
	    error "delmsg sec $sec"
	}
    }
    if {![llength [msgsdb_get $n inbound]]} {
	ucmdr "No incoming messages to delete." {}
    }
    tell_getcstate
    if {![info exists u]} {
	usererror \
 "I can't delete your messages unless I can see you on a channel with me.\
  Otherwise I might delete a message I hadn't told you about yet."
    }
    if {"$stt" != "passed"} {
	set telling [list $u undelivered 0]
	usererror \
 "There are message(s) you may not yet have seen;\
 I'll deliver them to you now.\
  If you actually want to delete them, just tell me `delmsg' again."
    }
    set senders [tell_peernicks $text]
    set ndel [tell_delete_msgs [irctolower $senders] [irctolower $n]]
    if {!$ndel} {
	if {[llength $senders]} {
	    ucmdr "No relevant incoming messages to delete." {}
	}
    }
    switch -exact [llength $senders] {
	0 { ucmdr {} {} "deletes your $ndel message(s)." }
	1 { ucmdr {} {} "deletes your $ndel message(s) from $senders." }
	default {
	    ucmdr {} {} "deletes your $ndel message(s) from\
 [lreplace $senders end end] and/or [lindex $senders end]."
	}
    }
}

def_ucmd tellme {
    prefix_nick
    ta_nomore
    check_notonchan
    manyset [nickdb_get $n tellsec] sec
    switch -exact $sec {
	reject { ucmdr "But, you asked me to reject messages for you !" {} }
	mailto { ucmdr "But, you asked me to mail your messages to you !" {} }
    }
    switch -exact [tell_event [irctolower $n] tellme] {
	ERROR - INVALID { ucmdr {} {is ill.  Help!} }
	nomsgs { ucmdr {You have no messages.} {} }
	default { }
    }
}

def_ucmd tell {
    global nick_case ownmailaddr ownfullname
    
    prefix_nick
    set target [ta_nick]
    if {![string length $text]} { error "tell them what?" }
    if {[string length $text] > 400} { error "message too long" }

    set ltarget [irctolower $target]
    set ctarget $target
    if {[info exists nick_case($ltarget)]} { set ctarget $nick_case($ltarget) }

    manyset [nickdb_get_sec_effective $target] sec mailtoint mailwhy
    manyset [nickdb_get $target tellrel] rel relint relwithin
    switch -exact $sec {
	insecure - secure {
	    set now [clock seconds]
	    set inbound [msgsdb_get $ltarget inbound]
	    lappend inbound $n $now $text
	    msgsdb_set $ltarget inbound $inbound

	    set outbound [msgsdb_get $n outbound]
	    set noutbound {}
	    set found 0
	    foreach {recip time count} $outbound {
		if {![ircnick_compare $recip $ltarget]} {
		    incr count
		    set recip $ctarget
		    set found 1
		}
		lappend noutbound $recip $time $count
	    }
	    if {!$found} {
		lappend noutbound $ctarget $now 1
	    }
	    msgsdb_set $n outbound $noutbound
	    set msg "OK, I'll tell $ctarget"
	    if {$found} { append msg " that too" }
	    append msg ", "
	    if {"$sec" != "secure"} {
		switch -exact $rel {
		    unreliable { append msg "neither reliably nor securely" }
		    remind { append msg "pretty reliably, but not securely" }
		    pester { append msg "reliably but not securely" }
		}
	    } else {
		switch -exact $rel {
		    unreliable { append msg "securely but not reliably" }
		    remind { append msg "securely and pretty reliably" }
		    pester { append msg "reliably and securely" }
		}
	    }
	    append msg .
	    tell_event $ltarget msgsarrive
	    ucmdr $msg {}
	}
	mailto {
	    set fmtmsg [exec fmt << " $text"]
	    exec /usr/sbin/sendmail -odb -oi -t -oee -f $mailwhy \
		    > /dev/null << \
 "From: $ownmailaddr ($ownfullname)
To: $mailtoint
Subject: IRC tell from $n

$n asked me[expr {[ischan $dest] ? " on $dest" : ""}] to tell you:
[exec fmt << " $text"]

(This message was for your nick $ctarget; your account $mailwhy
 arranged for it to be forwarded to $mailtoint.)
"
            ucmdr \
 "I've mailed $ctarget, which is what they prefer." \
                {}
	}
	reject {
	    usererror "Sorry, $ctarget does not want me to take messages."
	}
	default {
	    error "bad tellsec $sec"
	}
    }
}

def_ucmd who {
    if {[ta_anymore]} {
	set target [ta_nick]; ta_nomore
	set myself 1
    } else {
	prefix_nick
	set target $n
	set myself [expr {"$target" != "$n"}]
    }
    set ltarget [irctolower $target]
    upvar #0 nick_case($ltarget) ctarget
    set nshow $target
    if {[info exists ctarget]} {
	upvar #0 nick_onchans($ltarget) oc
	upvar #0 nick_username($ltarget) nu
	if {[info exists oc]} { set nshow $ctarget }
    }
    if {![nickdb_exists $ltarget]} {
	set ol "$nshow is not a registered nick."
    } elseif {[string length [set username [nickdb_get $target username]]]} {
	set ol "The nick $nshow belongs to the user $username."
    } else {
	set ol "The nick $nshow is registered (but not to a username)."
    }
    if {![info exists ctarget] || ![info exists oc]} {
	if {$myself} {
	    append ol "\nI can't see $nshow on anywhere."
	} else {
	    append ol "\nYou aren't on any channels with me."
	}
    } elseif {![info exists nu]} {
	append ol "\n$nshow has not identified themselves."
    } elseif {![info exists username]} {
	append ol "\n$nshow has identified themselves as the user $nu."
    } elseif {"$nu" != "$username"} {
	append ol "\nHowever, $nshow is being used by the user $nu."
    } else {
	append ol "\n$nshow has identified themselves to me."
    }
    ucmdr {} $ol
}

def_ucmd register {
    prefix_nick
    check_notonchan
    set old [nickdb_exists $n]
    if {$old} { nick_securitycheck 0 }
    set luser [irctolower $n]
    switch -exact [string tolower [string trim $text]] {
	{} {
	    upvar #0 nick_username($luser) nu
	    if {![info exists nu]} {
		ucmdr {} \
 "You must identify yourself before using `register'.  See `help identify', or use `register insecure'."
	    }
	    nickdb_set $n username $nu
	    ucmdr {} {} "makes a note of your username." {}
	}
	delete {
	    nickdb_delete $n
	    ucmdr {} {} "forgets your nickname." {}
	}
	insecure {
	    nickdb_set $n username {}
	    if {$old} {
		ucmdr {} "Security is now disabled for your nickname !"
	    } else {
		ucmdr {} "This is fine, but bear in mind that people will be able to mess with your settings.  Channel management features need a secure registration." "makes an insecure registration for your nick."
	    }
	}
	default {
	    error "you mean register / register delete / register insecure"
	}
    }
}

proc timeformat_desc {tf} {
    switch -exact $tf {
	ks { return "Times will be displayed in seconds or kiloseconds." }
	hms { return "Times will be displayed in hours, minutes, etc." }
	beat { return "Times will be displayed in beats (1000B = 1d)." }
	default { error "invalid timeformat: $v" }
    }
}

set settings {}
proc def_setting {opt show_body set_body} {
    global settings
    lappend settings $opt
    proc set_show/$opt {} "
        upvar 1 n n
        set opt $opt
        $show_body"
    if {![string length $set_body]} return
    proc set_set/$opt {} "
        upvar 1 n n
        upvar 1 text text
        set opt $opt
        $set_body"
}

proc tellme_sec_desc {v n} {
    manyset $v sec mailtoint
    switch -exact $sec {
	insecure {
	    return "I'll tell you your messages whenever I see you."
	}
	secure {
	    if {[string length [nickdb_get_username $n]]} {
		return \
 "I'll keep the bodies of your messages private until you identify yourself, reminding you every [showintervalsecs $mailtoint 1]."
	    } else {
		return \
 "I'll tell you your messages whenever I see you.\
  (Secure message delivery is enabled, but your nick is not registered\
 securely.  See `help register'.)"
	    }
	}
	reject {
	    return "I shan't accept messages for you."
	}
	mailto {
	    return "I'll forward your messages by email to $mailtoint."
	}
	default {
	    error "bad tellsec $sec"
	}
    }
}

proc tellme_rel_desc {v n} {
    manyset $v rel every within
    switch -exact $rel {
	unreliable {
	    return "As soon as I've told you message(s), I'll forget them\
 - note that this means messages can get lost !"
	}
	pester {
	    set u {}
	}
	remind {
	    set u ", or talk on channel within [showintervalsecs $within 1] of me having told you"
	}
	default {
	    error "bad tellrel $rel"
	}
    }
    return "After delivering messages, I'll remind you every\
 [showintervalsecs $every 1] until you say delmsg$u."
}

def_setting timeformat {
    set tf [nickdb_get $n timeformat]
    return "$tf: [timeformat_desc $tf]"
} {
    set tf [string tolower [ta_word]]
    ta_nomore
    set desc [timeformat_desc $tf]
    nickdb_set $n timeformat $tf
    ucmdr {} $desc
}

proc marktime_desc {mt} {
    switch -exact $mt {
	off {
	    return "I will not send you periodic messages."
	}
	once {
	    return "I will send you one informational message when I see you."
	}
	default {
	    return "I'll send you a message every [showintervalsecs $mt 0]."
	}
    }
}

def_setting marktime {
    set mt [nickdb_get $n marktime]
    set p $mt
    if {[string match {[0-9]*} $mt]} { append p s }
    append p ": "
    append p [marktime_desc $mt]
    return $p
} {
    global marktime_min
    set mt [string tolower [ta_word]]
    ta_nomore

    switch -exact $mt {
	off - once {
	}
	default {
	    set mt [parse_interval $mt $marktime_min]
	}
    }
    nickdb_set $n marktime $mt
    lnick_marktime_start [irctolower $n] "So:" 500 0
    ucmdr {} [marktime_desc $mt]
}

def_setting security {
    set s [nickdb_get $n username]
    if {[string length $s]} {
	return "Your nick, $n, is controlled by the user $s."
    } else {
	return "Your nick, $n, is not secure."
    }
} {}

proc tellme_setting_sec_simple {} {
    uplevel 1 {
	ta_nomore
	set sr sec
	set v $setting
    }
}

proc tellme_setting_neednomsgs {} {
    uplevel 1 {
	if {[llength [msgsdb_get $n inbound]]} {
	    usererror "You must delete the incoming messages you have, first."
	}
    }
}

def_setting tellme {
    set secv [nickdb_get $n tellsec]
    set ms [tellme_sec_desc $secv $n]
    manyset $secv sec
    switch -exact $sec {
	insecure - secure {
	    set mr [tellme_rel_desc [nickdb_get $n tellrel] $n]
	    return "$ms  $mr"
	}
	reject - mailto {
	    return $ms
	}
    }
} {
    set setting [string tolower [ta_word]]
    set nl [irctolower $n]
    switch -exact $setting {
	insecure {
	    tellme_setting_sec_simple
	}
	secure {
	    set every [ta_interval_optional 60 600]
	    ta_nomore
	    set sr sec
	    set v [list secure $every]
	}
	reject {
	    tellme_setting_neednomsgs
	    tellme_setting_sec_simple
	}
	mailto {
	    tellme_setting_neednomsgs
            
	    upvar #0 nick_username($nl) nu
	    if {!([info exists nu] && [string length $nu])} {
		usererror \
 "Sorry, you must register securely to have your messages mailed\
 (to prevent the use of this feature for spamming).  See `help register'."
	    }
	    set sr sec
	    set v [list mailto [ta_word] $nu]
	}
	unreliable - pester - remind {
	    manyset [nickdb_get $n tellsec] sec
	    switch -exact $sec {
		reject - mailto {
		    usererror \
 "Sorry, I shan't change when I'll consider a message delivered, because\
 you've asked me not to keep messages, or to mail them to you.\
  You should say `set tellme secure' or some such, first."
		}
	    }
	    set sr rel
	    set v $setting
	    if {"$setting" != "unreliable"} {
		set every [ta_interval_optional 300 3600]
		lappend v $every
	    }
	    if {![string compare $setting remind]} {
		set within [ta_interval_optional 5 30]
		if {$within > $every} {
		    error "remind interval must be at least time to respond"
		}
		lappend v $within
	    }
	    ta_nomore
	}
	default {
	    error "invalid tellme setting $setting"
	}
    }
    nickdb_set $n tell$sr $v
    upvar #0 nick_telling($nl) telling
    catch { unset telling }
    ucmdr [tellme_${sr}_desc $v $n] {}
}

proc lnick_checktold {luser} {
    set ml [msgsdb_get $luser outbound]
    if {![llength $ml]} return
    set is1 [expr {[llength $ml]==3}]
    set m1 "FYI, I haven't yet delivered your"
    set ol {}
    set now [clock seconds]
    while {[llength $ml]} {
	manyset $ml r t n
	set ml [lreplace $ml 0 2]
	set td [expr {$now-$t}]
	if {$n == 1} {
	    set iv [showinterval $td]
	    set ifo "$r, $iv"
	    set if1 "message to $r, $iv."
	} else {
	    set iv [showintervalsecs $td 0]
	    set ifo "$r, $n messages, oldest $iv"
	    set if1 "$n messages to $r, oldest $iv."
	}
	if {$is1} {
	    sendprivmsg $luser "$m1 $if1"
	    return
	} else {
	    lappend ol " to $ifo[expr {[llength $ml] ? ";" : "."}]"
	}
    }
    sendprivmsg $luser "$m1 messages:"
    msendprivmsg $luser $ol
}

def_ucmd set {
    global settings
    prefix_nick
    check_notonchan
    if {![nickdb_exists $n]} {
	ucmdr {} "You are unknown to me and so have no settings.  (Use `register'.)"
    }
    if {![ta_anymore]} {
	set ol {}
	foreach opt $settings {
	    lappend ol [format "%-10s %s" $opt [set_show/$opt]]
	}
	ucmdr {} [join $ol "\n"]
    } else {
	set opt [ta_word]
	if {[catch { info body set_show/$opt }]} {
	    error "no setting $opt"
	}
	if {![ta_anymore]} {
	    ucmdr {} "$opt: [set_show/$opt]"
	} else {
	    nick_securitycheck 0
	    if {[catch { info body set_set/$opt }]} {
		error "setting $opt cannot be set with `set'"
	    }
	    set_set/$opt
	}
    }
}

def_ucmd identpass {
    prefix_nick
    check_notonchan
    set luser [irctolower $n]
    set username [ta_word]
    set passmd5 [md5sum "[ta_word]\n"]
    ta_nomore
    upvar #0 nick_onchans($luser) onchans
    if {![info exists onchans] || ![llength $onchans]} {
	ucmdr "You must be on a channel with me to identify yourself." {}
    }
    check_username $username
    exec userv --timeout 3 $username << "$passmd5\n" > /dev/null \
	    irc-identpass $n
    upvar #0 nick_username($luser) rec_username
    set rec_username $username
    after 50 [list tell_event $luser ident]
    ucmdr "Pleased to see you, $username." {}
}

def_ucmd kill {
    global nick
    prefix_nick
    set target [ta_nick]
    if {![nickdb_exists $target]} { error "$target is not a registered nick." }
    set wantu [nickdb_get $target username]
    if {![string length $wantu]} { error "$target is insecurely registred." }
    upvar #0 nick_username([irctolower $n]) nu
    if {![info exists nu]} { error "You must identify yourself first." }
    if {"$wantu" != "$nu"} {
	error "You are the wrong user, $nu - $target belongs to $wantu."
    }
    set reason "at request of user $nu"
    if {[ta_anymore]} { append reason "; $text" }
    sendout KILL $target $reason
    ucmdr {} {}
}

def_ucmd summon {
    set target [ta_word]
    ta_nomore
    # fixme would be nice if the rest of the text was passed on instead
    check_username $target
    prefix_nick

    upvar #0 lastsummon($target) ls
    set now [clock seconds]
    if {[info exists ls]} {
	set interval [expr {$now - $ls}]
	if {$interval < 30} {
	    ucmdr {} \
 "Please be patient; $target was summoned only [showinterval $interval]."
	}
    }
    regsub {^[^!]*!} $p {} path
    if {[catch {
	exec userv --timeout 3 $target irc-summon $n $path \
		[expr {[ischan $dest] ? "$dest" : ""}] \
		< /dev/null
    } rv]} {
	regsub -all "\n" $rv { / } rv
	error $rv
    }
    if {[regexp {^problem (.*)} $rv dummy problem]} {
	ucmdr {} "The user `$target' $problem."
    } elseif {[regexp {^ok ([^ ]+) ([0-9]+)$} $rv dummy tty idlesince]} {
	set idletime [expr {$now - $idlesince}]
	set ls $now
	ucmdr {} {} {} "invites $target ($tty[expr {
	    $idletime > 10 ? ", idle for [showintervalsecs $idletime 0]" : ""
	}]) to [expr {
	    [ischan $dest] ? "join us here" : "talk to you"
	}]."
    } else {
	error "unexpected response from userv service: $rv"
    }
}

proc md5sum {value} { exec md5sum << $value }

def_ucmd seen {
    global lastseen nick
    prefix_nick
    set ncase [ta_nick]
    set nlower [irctolower $ncase]
    ta_nomore
    set now [clock seconds]
    if {![ircnick_compare $nlower $nick]} {
	usererror "I am not self-aware."
    } elseif {![info exists lastseen($nlower)]} {
	set rstr "I've never seen $ncase."
    } else {
	manyset $lastseen($nlower) realnick time what
	set howlong [expr {$now - $time}]
	set string [showinterval $howlong]
	set rstr "I last saw $realnick $string, $what."
    }
    if {[ischan $dest]} {
	set where $dest
    } else {
	set where {}
    }
    upvar #0 lookedfor($nlower) lf
    if {[info exists lf]} { set oldvalue $lf } else { set oldvalue {} }
    set lf [list [list $now $n $where]]
    foreach v $oldvalue {
	if {![ircnick_compare [lindex $v 1] $n]} continue
	lappend lf $v
    }
    ucmdr {} $rstr
}

proc lnick_marktime_cancel {luser} {
    upvar #0 nick_markid($luser) mi
    if {![info exists mi]} return
    catch { after cancel $mi }
    catch { unset mi }
}

proc lnick_marktime_doafter {luser why ms mentiontold} {
    lnick_marktime_cancel $luser
    upvar #0 nick_markid($luser) mi
    set mi [after $ms [list lnick_marktime_now $luser $why 0]]
}

proc lnick_marktime_reset {luser} {
    set mt [nickdb_get $luser marktime]
    switch -exact $mt {
	off - once { }
	default {
	    lnick_marktime_doafter $luser "Time passes." [expr {$mt*1000}] 0
	}
    }
}

proc lnick_marktime_start {luser why ms mentiontold} {
    set mt [nickdb_get $luser marktime]
    switch -exact $mt {
	off {
	    lnick_marktime_cancel $luser
	    if {$mentiontold} { after $ms [list lnick_checktold $luser] }
	}
	default {
	    lnick_marktime_doafter $luser $why $ms $mentiontold
	}
    }
}

proc lnick_marktime_now {luser why mentiontold} {
    upvar #0 nick_onchans($luser) oc
    global calling_nick
    set calling_nick $luser
    sendprivmsg $luser [lnick_pingstring $why $oc ""]
    if {$mentiontold} { after 150 [list lnick_checktold $luser] }
    lnick_marktime_reset $luser
}    

proc lnick_pingstring {why oc apstring} {
    global nick_onchans
    catch { exec uptime } uptime
    set nnicks [llength [array names nick_onchans]]
    if {[regexp \
 {^ *([0-9:apm]+) +up.*, +(\d+) users?, +load average: +([0-9., ]+) *$} \
            $uptime dummy time users load]} {
	regsub -all , $load {} load
        set uptime "$time  $nnicks/$users  $load"
    } else {
	append uptime ", $nnicks nicks"
    }
    if {[llength $oc]} {
	set best_la 0
	set activity quiet
	foreach ch $oc {
	    upvar #0 chan_lastactivity($ch) la
	    if {![info exists la]} continue
	    if {$la <= $best_la} continue
	    set since [showintervalsecs [expr {[clock seconds]-$la}] 1]
	    set activity "$ch $since"
	    set best_la $la
	}
    } else {
	set activity unseen
    }
    set str $why
    append str "  " $uptime "  " $activity
    if {[string length $apstring]} { append str "  " $apstring }
    return $str
}

def_ucmd ping {
    prefix_nick
    set ln [irctolower $n]
    if {[ischan $dest]} {
	set oc [irctolower $dest]
    } else {
	global nick_onchans
	if {[info exists nick_onchans($ln)]} {
	    set oc $nick_onchans($ln)
	} else {
	    set oc {}
	}
	if {[llength $oc]} { lnick_marktime_reset $ln }
    }
    after 150 [list lnick_checktold $ln]
    ucmdr {} [lnick_pingstring "Pong!" $oc $text]
}

proc ensure_globalsecret {} {
    global globalsecret
    
    if {[info exists globalsecret]} return
    set gsfile [open /dev/urandom r]
    fconfigure $gsfile -translation binary
    set globalsecret [read $gsfile 32]
    binary scan $globalsecret H* globalsecret
    close $gsfile
    unset gsfile
}

proc connected {} {
    global operuserpass
    if {[info exists operuserpass]} {
	eval sendout OPER $operuserpass
    }
    foreach chan [chandb_list] {
	if {[chandb_get $chan autojoin]} { dojoin $chan }
    }
}

ensure_globalsecret
loadhelp
ensure_connecting
