#!/usr/bin/tclsh8.2
# usage:
#   cd ../ircbot
#   soemthing | ./spoutchan.tcl SERVER PORT NICK IDENT FULLNAME CHANNEL

source irccore.tcl

manyset $argv host port nick ident ownfullname channel

proc connected {} {
    global channel
    sendout JOIN $channel
}
proc new_event {} { }
proc privmsg_unlogged {args} { return 1 }
proc msg_366 {args} {
    fconfigure stdin -blocking no
    fileevent stdin readable stdinread
}
proc stdinread {} {
    global channel
    if {[eof stdin]} { exit 0 }
    if {[gets stdin l] < 0} return
    sendprivmsg $channel $l
}

ensure_connecting
vwait forever
