# Configuration for testbot

set host chiark
set nick bnbridge
set ident $nick
set ownfullname confused
set socketargs {}
set marktime_min 10
set channel #starcraft

set bots {bw sc}
source botpass.tcl

set bot/sc/host bnetd.relativity.greenend.org.uk
set bot/sc/nick scbridge
set bot/sc/pass $botpass
set bot/sc/channel "Starcraft"

set bot/bw/host bnetd.relativity.greenend.org.uk
set bot/bw/nick bwbridge
set bot/bw/pass $botpass
set bot/bw/channel "Brood War"

set bnbot bnbot

source bridge.tcl
