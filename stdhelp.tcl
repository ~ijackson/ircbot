proc loadhelp {} {
    global help_topics errorInfo helpfile

    catch { unset help_topics }
    set f [open $helpfile r]
    try_except_finally {
	set lno 0
	while {[gets $f l] >= 0} {
	    incr lno
	    if {[regexp {^#.*} $l]} {
	    } elseif {[regexp {^ *$} $l]} {
		if {[info exists topic]} {
		    set help_topics($topic) [join $lines "\n"]
		    unset topic
		    unset lines
		}
	    } elseif {[regexp {^\:\:} $l]} {
	    } elseif {[regexp {^\:([-+._0-9a-z]*)$} $l dummy newtopic]} {
		if {[info exists topic]} {
		    error "help $newtopic while in $topic"
		}
		set topic $newtopic
		set lines {}
	    } elseif {[regexp {^[^:#]} $l]} {
		set topic
		regsub -all {([^\\])\!\$?} _$l {\1} l
		regsub -all {\\(.)} $l {\1} l
		regsub {^_} $l {} l
		lappend lines [string trimright $l]
	    } else {
		error "eh ? $lno: $l"
	    }
	}
	if {[info exists topic]} { error "unfinished topic $topic" }
    } {
	set errorInfo "in $helpfile line $lno\n$errorInfo"
    } {
	close $f
    }
}

def_ucmd help {
    upvar 1 n n

    set topic [irctolower [string trim $text]]
    if {[string length $topic]} {
	set ontopic " on `$topic'"
    } else {
	set ontopic ""
    }
    if {[set lag [out_lagged]]} {
	if {[ischan $dest]} { set replyto $dest } else { set replyto $n }
	if {$lag > 1} {
	    sendaction_priority 1 $replyto \
		"is very lagged.  Please ask for help$ontopic again later."
	    ucmdr {} {}
	} else {
	    sendaction_priority 1 $replyto \
		"is lagged.  Your help$ontopic will arrive shortly ..."
	}
    }
    
    upvar #0 help_topics($topic) info
    if {![info exists info]} { ucmdr "No help on $topic, sorry." {} }
    ucmdr $info {}
}

def_ucmd ? {
    global help_topics
    ucmdr $help_topics() {}
}

