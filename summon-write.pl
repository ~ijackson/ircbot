#!/usr/bin/perl

sub fault ($) { print STDERR "$_[0]\n"; exit 8; }

@ARGV==5 or fault("invalid arguments to $0");
($line,$idlesince,$nick,$path,$chan) = @ARGV;

open X, ">&0" or fault("reopen stdin for output: $!");

$timestring= localtime time;

pline("\a");
pline("You are cordially invited onto the system's IRC server by \`$nick'");
pline("($path) at $timestring");
pline("If you don't know how to use IRC, run \`irc <your-nickname> chiark'");
pline("When you are on-line, please ".
      (length $chan ? "join $chan." : "send a private note to $nick."));
pline("");

close X or outfail();

print "ok $line $idlesince\n";
exit 0;

sub pline ($) {
    my ($s) = @_;
    print X "\r", " "x78, "\r $s\r\n" or outfail();
}

sub outfail () {
    if ($! == &EAGAIN || $! == &EWOULDBLOCK) {
	print "failed has a congested terminal\n";
	exit 0;
    } else {
	print "failed has a broken terminal ($!)\n";
	exit 0;
    }
}
