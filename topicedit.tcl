#!/usr/bin/tclsh8.5
# usage:
#   cd ../ircbot
#   ./topicedit.tcl SERVER PORT NICK IDENT FULLNAME CHANNEL TOPICINFO

source irccore.tcl

manyset $argv host port nick ident ownfullname channel topicinfo

set done 0

after 30000 { set errorInfo {}; bgerror timeout }

proc bgerror {msg} {
    global errorInfo errorCode
    puts stderr "$msg\n$errorCode\n$errorInfo\n\nERROR: $msg\n"
    exit 16
}

proc connected {} {
    global channel
    sendout JOIN $channel
}
proc new_event {} { }
proc privmsg_unlogged {args} { return 1 }
proc msg_332 {server code us channel topic args} {
    global oldtopic nick
    if {[string compare $nick $us]} return
    set oldtopic $topic
}
proc msg_333 {server code us channel setter when} {
    global nick done
    # might happen as a result of us connecting, or as a result of
    # our own TOPIC query
    set now [clock seconds]
    if {![string compare $nick $setter] && $when > $now - 3600} {
	if {$done} {
	    puts "topic set."
	    exit 0
	} else {
	    puts stderr "*** topic recently set by us, not setting again!"
	    exit 1
	}
    }
}
proc msg_366 {args} {
    global oldtopic topicinfo channel done
    if {![info exists oldtopic]} return
    set newtopic "$topicinfo | $oldtopic"
    sendout TOPIC $channel $newtopic
    sendout TOPIC $channel
    set done 1
}

ensure_connecting
vwait forever
