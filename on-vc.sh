#!/bin/sh
# usage:
#    .../on-vc.sh VC SCRIPT
# must be invoked by full path to relevant directory
# where
#    ttyVC is the relevant tty which should have good permissions
#    ./SCRIPT is the startup script (`repeatedly.sh', `startup.sh', etc.)
#      (in the directory whose fullpathname we're invoked with)

set -e

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

fail () { echo 2>&1 "$0: $1"; exit 1; }

test $# -ge 2 || fail "usage: $0 VC SCRIPT [ARGS]"
vc="$1"
script="$2"
shift; shift

cd "$(dirname $0)"
test -L .tclshrc || fail "no .tclshrc symlink"
test -x "./$script" || fail "script not executable"

openvt -f -c "$vc" -w -- screen ./"$script" "$@"
