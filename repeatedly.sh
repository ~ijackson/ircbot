#!/bin/sh
set -e
cd "`dirname $0`"

while true; do
	date
	set +e
	HOME=. alarm 50000 tclsh8.2
	rc=$?
	set -e
	date
	test $rc = 142 || sleep 590
	sleep 10
done
