# Configuration for testbot

set host chiark
set nick testbot
set ownfullname confused
set socketargs {}
set marktime_min 10
set channel #test

set bots bw
source botpass.tcl

set bot/bw/host bnetd.relativity.greenend.org.uk
set bot/bw/nick iwj-test1
set bot/bw/pass $botpass
set bot/bw/channel "Brood War"

set bnbot ./bnbot

source bridge.tcl
