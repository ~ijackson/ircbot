#!/bin/sh
set -e
cd "`dirname $0`"
sleep 1
set +e
./blight-startup.tcl
sleep ${1:-5000000}
