proc ta_anymore {} {
    upvar 1 text text
    return [expr {!![string length $text]}]
}

proc ta_nomore {} {
    upvar 1 text text
    if {[string length $text]} { error "too many parameters" }
}

proc ta_word {} {
    upvar 1 text text
    if {![regexp {^([^ 	]+) *(.*)} $text dummy firstword text]} {
	error "too few parameters"
    }
    return $firstword
}

proc ta_nick {} {
    upvar 1 text text
    set v [ta_word]
    check_nick $v
    return $v
}

proc ta_interval_optional {min def} {
    upvar 1 text text
    if {[ta_anymore]} {
	return [parse_interval [ta_word] $min]
    } else {
	return $def
    }
}

proc usererror {emsg} { error $emsg {} {BLIGHT USER} }

proc go_usercommand {p c n dest text} {
    regsub {^! *} $text {} text
    set ucmd [ta_word]
    set procname ucmd/[string tolower $ucmd]
    if {[catch { info body $procname }]} {
	usererror "Unknown command; try help for Help."
    }
    $procname $p $dest
}

proc execute_usercommand {p c n output dest text} {
    global errorCode
    if {[catch {
	go_usercommand $p $c $n $dest $text
    } rv]} {
	if {"$errorCode" != "BLIGHT USER"} { set rv "error: $rv" }
	sendprivmsg $n $rv
    } else {
	manyset $rv priv_msgs pub_msgs priv_acts pub_acts
	foreach {td val} [list $n $priv_acts $output $pub_acts] {
	    foreach l [split $val "\n"] {
		sendaction_priority 0 $td $l
	    }
	}
	foreach {td val} [list $n $priv_msgs $output $pub_msgs] {
	    foreach l [split $val "\n"] {
		sendprivmsg $td $l
	    }
	}
    }
}

proc def_ucmd {cmdname body} {
    proc ucmd/$cmdname {p dest} "    upvar 1 text text\n$body"
}

proc def_ucmd_alias {alias canon} {
    proc ucmd/$alias {p dest} "    uplevel 1 \[list ucmd/$canon \$p \$dest\]\n"
}

proc ucmdr {priv pub args} {
    return -code return [concat [list $priv $pub] $args]
}

proc new_event {} {
    global errorInfo errorCode
    set ei $errorInfo
    set ec $errorCode
    catch { unset calling_nick }
    set errorInfo $ei
    set errorCode $ec
}
