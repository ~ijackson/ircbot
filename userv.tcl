
proc check_username {target} {
    if {
	[string length $target] > 8 ||
        [regexp {[^-0-9a-z]} $target] ||
        ![regexp {^[a-z]} $target]
    } { error "invalid username" }
}
