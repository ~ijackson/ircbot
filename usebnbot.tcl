# Code for starting up bnbot

proc def_bnbot {name argl body} {
    proc "bnbot_$name" [concat botid $argl] \
    "bnbot__vars\n
    $body"
}

proc bnbot__vars {} {
    global bnbot_callervars
    upvar 1 botid botid
    foreach v [concat {
	host port nick pass channel
	chan mbokafter state chanfn
    } $bnbot_callervars] {
	uplevel 1 [list upvar #0 "bot/$botid/$v" bn$v]
    }
}

def_bnbot ensure_connecting {} {
    global muststartby_ms bnbot
    
    if {[info exists bnchan]} return
    defset bnport 6112
    set bnchan [open [list | $bnbot $bnhost $bnport] w+]
    fconfigure $bnchan -buffering line
    set bnmbokafter [after $muststartby_ms \
	    "fail {bot $botid not ok within timeout}"]
    set bnstate Connected
    fileevent $bnchan readable [list bnbot_onread $botid]
}

def_bnbot write {str} {
    log "[clock seconds] -$botid-> $str"
    puts $bnchan $str
}

def_bnbot writemsg {str} {
    if {[regexp {^/} $str]} { set str " $str" }
    bnbot_write $botid $str
}

def_bnbot onread {args} {
    global channel
    if {[gets $bnchan l] == -1} { fail "bot $botid EOF/error on input" }
    if {[regexp {^1005 TALK ([^ ]+) \w+ \"(.*)\"$} $l dummy n text]} {
	sendprivmsg $channel "\[$n] $text"
	return
    } elseif {[regexp {^1023 EMOTE ([^ ]+) \w+ \"(.*)\"$} $l dummy n text]} {
	if {![ircnick_compare $n $bnnick]} return
	sendprivmsg $channel "* $n $text"
	return
    }
    log "[clock seconds] <-$botid- $l"
    if {[string length $bnstate] && [regexp "^$bnstate" $l]} {
	switch -exact $bnstate {
	    Connected { set bnstate Username }
	    Username { set bnstate Password; bnbot_write $botid $bnnick }
	    Password {
		set bnstate "1007 CHANNEL"
		puts $bnchan $bnpass
	    }
	    {1007 CHANNEL} {
		set bnstate {}
		bnbot_write $botid "/CHANNEL $bnchannel"
	    }
	    default { error "wrong bnstate: $bnstate" }
	}
    } elseif {[regexp {^1007 CHANNEL "(.*)"} $l dummy bnchanfn]} {
	after cancel $bnmbokafter
	unset bnmbokafter
    } elseif {[info exists bnchanfn]} {
	bnbot_event $botid $l
    }
}
